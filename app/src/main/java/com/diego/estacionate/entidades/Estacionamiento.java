package com.diego.estacionate.entidades;

import com.google.firebase.database.Exclude;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Diego on 06-11-2016.
 */

public class Estacionamiento {

    private String id_estacionamiento;
    private String nombre;
    private String direccion;
    private String capacidad;
    private String horario;
    private String tipo_estacionamiento;
    private String altura;
    private String precio;
    private String fecha_uso;
    private String estado;
    private String id_usuario;
    private String path_img;
    private String patente;
    private String geoX;
    private String geoY;

    public String getGeoX() {
        return geoX;
    }

    public void setGeoX(String geoX) {
        this.geoX = geoX;
    }

    public String getGeoY() {
        return geoY;
    }

    public void setGeoY(String geoY) {
        this.geoY = geoY;
    }

    public String getPatente() {
        return patente;
    }

    public void setPatente(String patente) {
        this.patente = patente;
    }

    public String getPath_img() {
        return path_img;
    }

    public void setPath_img(String path_img) {
        this.path_img = path_img;
    }

    public String getId_usuario() {
        return id_usuario;
    }

    public void setId_usuario(String id_usuario) {
        this.id_usuario = id_usuario;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getFecha_uso() {
        return fecha_uso;
    }

    public void setFecha_uso(String fecha_uso) {
        this.fecha_uso = fecha_uso;
    }

    public String getId_estacionamiento() {
        return id_estacionamiento;
    }

    public void setId_estacionamiento(String id_estacionamiento) {
        this.id_estacionamiento = id_estacionamiento;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getCapacidad() {
        return capacidad;
    }

    public void setCapacidad(String capacidad) {
        this.capacidad = capacidad;
    }

    public String getHorario() {
        return horario;
    }

    public void setHorario(String horario) {
        this.horario = horario;
    }

    public String getTipo_estacionamiento() {
        return tipo_estacionamiento;
    }

    public void setTipo_estacionamiento(String tipo_estacionamiento) {
        this.tipo_estacionamiento = tipo_estacionamiento;
    }

    public String getAltura() {
        return altura;
    }

    public void setAltura(String altura) {
        this.altura = altura;
    }

    public String getPrecio() {
        return precio;
    }

    public void setPrecio(String precio) {
        this.precio = precio;
    }

    public Estacionamiento() {
    }

    public Estacionamiento(String id_estacionamiento, String nombre, String direccion, String capacidad, String horario, String tipo_estacionamiento, String altura, String precio) {
        setId_estacionamiento(id_estacionamiento);
        setNombre(nombre);
        setDireccion(direccion);
        setCapacidad(capacidad);
        setHorario(horario);
        setTipo_estacionamiento(tipo_estacionamiento);
        setAltura(altura);
        setPrecio(precio);
    }

    @Exclude
    public Map<String, Object> toMap() {
        HashMap<String, Object> result = new HashMap<>();
        result.put("id_estacionamiento", getId_estacionamiento());
        result.put("nombre", getNombre());
        result.put("direccion", getDireccion());
        result.put("capacidad", getCapacidad());
        result.put("horario", getHorario());
        result.put("tipo_estacionamiento", getTipo_estacionamiento());
        result.put("altura", getAltura());
        result.put("precio", getPrecio());
        result.put("estado", getEstado());
        result.put("id_usuario", getId_usuario());
        result.put("path_img",getPath_img());
        result.put("geoX",getGeoX());
        result.put("geoY",getGeoY());
        return result;
    }

    @Exclude
    public Map<String, Object> toMapFavorito() {
        HashMap<String, Object> result = new HashMap<>();
        result.put("id_estacionamiento", getId_estacionamiento());
        result.put("nombre", getNombre());
        result.put("direccion", getDireccion());
        result.put("id_usuario", getId_usuario());
        return result;
    }

    @Exclude
    public Map<String, Object> toMapHistorial() {
        HashMap<String, Object> result = new HashMap<>();
        result.put("id_estacionamiento", getId_estacionamiento());
        result.put("nombre", getNombre());
        result.put("direccion", getDireccion());
        result.put("fecha_uso",getFecha_uso());
        result.put("patente",getPatente());

        return result;
    }

    @Exclude
    public Map<String, Object> toMapEstado() {
        HashMap<String, Object> result = new HashMap<>();
        result.put("estado", getEstado());

        return result;
    }



}
