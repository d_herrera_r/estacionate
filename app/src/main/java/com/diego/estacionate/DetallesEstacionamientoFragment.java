package com.diego.estacionate;


import android.app.AlertDialog;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.diego.estacionate.entidades.Estacionamiento;
import com.diego.estacionate.entidades.Usuario;
import com.diego.estacionate.entidades.Vehiculo;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.squareup.picasso.Picasso;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;




/**
 * A simple {@link Fragment} subclass.
 */
public class DetallesEstacionamientoFragment extends Fragment{

    private ProgressDialog mProgressDialog;

    Button btnUsar, btnSolicitarAct, btnAgregarFavorito;
    TextView tvNombre, tvDireccion, tvCapacidad, tvHorario, tvTipo, tvAltura, tvPrecio, textoCargando;
    ImageView imgEst;

    private DatabaseReference mDatabase;
    private FirebaseStorage storage;
    private StorageReference storageRef;
    private static final String TAG ="lalaa";
    private Bundle datos;

    public DetallesEstacionamientoFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_detalles_estacionamiento, container, false);

        showProgressDialog();

        datos = getArguments();


        imgEst = (ImageView)view.findViewById(R.id.imgEst);
        tvNombre = (TextView) view.findViewById(R.id.tvNombre);
        tvDireccion = (TextView) view.findViewById(R.id.tvDireccion);
        tvCapacidad = (TextView) view.findViewById(R.id.tvCapacidad);
        tvHorario = (TextView) view.findViewById(R.id.tvHorario);
        tvTipo = (TextView) view.findViewById(R.id.tvTipo);
        tvAltura = (TextView) view.findViewById(R.id.tvAltura);
        tvPrecio = (TextView) view.findViewById(R.id.tvPrecio);
        textoCargando = (TextView) view.findViewById(R.id.textoCargando);


        mDatabase = FirebaseDatabase.getInstance().getReference();

        mDatabase.child("estacionamientos").child(datos.getString("uid")).addValueEventListener(
                new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        // Get user value
                        Estacionamiento est = dataSnapshot.getValue(Estacionamiento.class);
                        tvNombre.setText(est.getNombre());
                        tvDireccion.setText(est.getDireccion());
                        tvCapacidad.setText(est.getCapacidad()+" vehiculos");
                        tvHorario.setText(est.getHorario());
                        tvTipo.setText(est.getTipo_estacionamiento());
                        tvAltura.setText(est.getAltura()+" mts altura máxima");
                        if(est.getAltura().equals("0")){ tvAltura.setText("Sin límite de altura"); }
                        tvPrecio.setText(est.getPrecio());
                        if(est.getPrecio().equals("0")){ tvPrecio.setText("Gratis"); }
                        if(est.getPath_img().equals("")){
                            storage = FirebaseStorage.getInstance();
                            storageRef = storage.getReferenceFromUrl("gs://estacionate-f78ce.appspot.com");
                            StorageReference baseImagen = storageRef.child("estacionamientos/base.jpg");
                            baseImagen.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {

                                @Override
                                public void onSuccess(Uri uri) {

                                    Picasso.with(getActivity()).load(uri.toString())
                                            .fit()
                                            .centerInside()
                                            .into(imgEst, new com.squareup.picasso.Callback() {
                                        @Override
                                        public void onSuccess() {
                                            textoCargando.setVisibility(View.INVISIBLE);
                                            hideProgressDialog();
                                        }

                                        @Override
                                        public void onError() {

                                        }
                                    });
                                    textoCargando.setVisibility(View.INVISIBLE);
                                }
                            }).addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception e) {
                                    Toast.makeText(getActivity(), "Error al cargar imagen", Toast.LENGTH_SHORT).show();
                                }
                            });
                        }else{
                            Picasso.with(getActivity()).load(est.getPath_img()).into(imgEst, new com.squareup.picasso.Callback() {
                                @Override
                                public void onSuccess() {
                                    textoCargando.setVisibility(View.INVISIBLE);
                                    hideProgressDialog();
                                }

                                @Override
                                public void onError() {

                                }
                            });

                        }

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        Log.w(TAG, "getUser:onCancelled", databaseError.toException());
                        // ...
                    }
                });


        btnUsar = (Button)view.findViewById(R.id.btnUsar);
        //btnSolicitarAct = (Button) view.findViewById(R.id.btnSolicitarAct);
        btnAgregarFavorito = (Button) view.findViewById(R.id.btnAgregarFavorito);

        mDatabase.child("favoritos")
                .child(FirebaseAuth.getInstance().getCurrentUser().getUid())
                .child(datos.getString("uid"))
                .addListenerForSingleValueEvent(
                new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        if(dataSnapshot.getValue(Estacionamiento.class)!=null){
                            btnAgregarFavorito.setEnabled(false);
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        Log.w(TAG, "getUser:onCancelled", databaseError.toException());
                        // ...
                    }
                });

        btnUsar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                final ArrayList<String> vehiculos = new ArrayList<String>();

                mDatabase.child("vehiculos")
                        .child(FirebaseAuth.getInstance().getCurrentUser().getUid()).addListenerForSingleValueEvent(
                        new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {
                                // Get user value
                                for(DataSnapshot temp : dataSnapshot.getChildren()){
                                    Vehiculo vehiculo = temp.getValue(Vehiculo.class);
                                    vehiculos.add(vehiculo.getPatente());
                                }

                                final CharSequence vehi[] = vehiculos.toArray(new CharSequence[vehiculos.size()]);
                                builder .setTitle("Selecciona un vehículo")
                                        .setSingleChoiceItems(vehi, 0, null)
                                        .setPositiveButton("Usar", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {



                                                int pos = ((AlertDialog)dialog).getListView().getCheckedItemPosition();


                                                DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
                                                Date date = new Date();

                                                String uid = FirebaseAuth.getInstance().getCurrentUser().getUid();

                                                Estacionamiento est = new Estacionamiento();
                                                est.setId_estacionamiento(datos.getString("uid"));
                                                est.setNombre(tvNombre.getText().toString());
                                                est.setDireccion(tvDireccion.getText().toString());
                                                est.setFecha_uso(dateFormat.format(date));
                                                est.setPatente(vehi[pos].toString());
                                                String key = mDatabase.child("historial").child(uid).push().getKey();



                                                Map<String, Object> childUpdates = new HashMap<>();
                                                childUpdates.put("/historial/" +
                                                        uid+"/"+
                                                        key, est.toMapHistorial());
                                                mDatabase.updateChildren(childUpdates);

                                                Toast.makeText(getActivity(), "Agregado a tu lista de usados", Toast.LENGTH_SHORT).show();
                                                dialog.dismiss();

                                            }
                                        })
                                        .setNegativeButton("Cancelar",
                                        new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int id) {
                                                dialog.dismiss();
                                            }
                                        });
                                builder.show();

                                // ...
                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {
                                Log.w(TAG, "getUser:onCancelled", databaseError.toException());
                                // ...
                            }
                        });




            }
        });
/*
        btnSolicitarAct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LayoutInflater li = LayoutInflater.from(getActivity());
                ScrollView act_info = (ScrollView) li.inflate(R.layout.alert_actualizar_info, null);

                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder .setTitle("Ingresar información actualizada")
                        .setView(act_info)
                        .setCancelable(false)
                        .setNegativeButton("Cancelar",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.dismiss();
                                    }
                                })
                        .setPositiveButton("Enviar info.",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {

                                    }
                                });
                AlertDialog alert = builder.create();
                alert.show();
            }
        });
*/

        btnAgregarFavorito.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Estacionamiento est = new Estacionamiento();
                est.setId_estacionamiento(datos.getString("uid"));
                est.setNombre(tvNombre.getText().toString());
                est.setDireccion(tvDireccion.getText().toString());
                est.setId_usuario(FirebaseAuth.getInstance().getCurrentUser().getUid());



                Map<String, Object> childUpdates = new HashMap<>();
                childUpdates.put("/favoritos/" +
                        FirebaseAuth.getInstance().getCurrentUser().getUid()+"/"+
                        est.getId_estacionamiento(), est.toMapFavorito());
                mDatabase.updateChildren(childUpdates);

                Toast.makeText(getActivity(), "Agregado a tus favoritos", Toast.LENGTH_SHORT).show();
                btnAgregarFavorito.setEnabled(false);
            }
        });

        return view;
    }

    public void showProgressDialog() {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(getActivity());
            mProgressDialog.setCancelable(false);
            mProgressDialog.setMessage("Cargando...");
        }

        mProgressDialog.show();
    }

    public void hideProgressDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }


}
