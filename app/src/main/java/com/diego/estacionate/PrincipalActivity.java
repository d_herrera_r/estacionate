package com.diego.estacionate;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.diego.estacionate.entidades.Estacionamiento;
import com.diego.estacionate.entidades.Usuario;
import com.facebook.login.LoginManager;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserInfo;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;

public class PrincipalActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private static final String TAG = "PrincipalUser";
    Toolbar toolbar;
    DrawerLayout drawer;
    NavigationView navigationView;
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    private TextView tv_nombre_sesion, tv_correo_sesion;
    private CircleImageView img_sesion;
    private DatabaseReference mDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_principal);

        mAuth = FirebaseAuth.getInstance();

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        View header=navigationView.getHeaderView(0);


        tv_nombre_sesion= (TextView)header.findViewById(R.id.tv_nombre_sesion);
        tv_correo_sesion=(TextView)header.findViewById(R.id.tv_correo_sesion);
        img_sesion=(CircleImageView)header.findViewById(R.id.img_sesion);

        tv_nombre_sesion.setText(mAuth.getCurrentUser().getDisplayName());
        for (UserInfo profile : mAuth.getCurrentUser().getProviderData()) {
            // Id of the provider (ex: google.com)
            String providerId = profile.getProviderId();
            if(providerId.equals("facebook.com")){
                navigationView.getMenu().findItem(R.id.menu_actualizar_contrasena).setVisible(false);
            }
        };
        tv_correo_sesion.setText(mAuth.getCurrentUser().getEmail());
        if(mAuth.getCurrentUser().getEmail().equals("diegoh233@gmail.com")){
            navigationView.getMenu().findItem(R.id.menu_aprobar_estacionamiento).setVisible(true);
        }
        Picasso.with(this)
                .load(mAuth.getCurrentUser().getPhotoUrl())
                .fit()
                .centerCrop()
                .into(img_sesion);

        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user != null) {
                    // User is signed in
                    Log.d(TAG, "onAuthStateChanged:signed_in:" + user.getUid());

                } else {
                    // User is signed out
                    Log.d(TAG, "onAuthStateChanged:signed_out");
                    Toast.makeText(PrincipalActivity.this, "Sesión cerrada", Toast.LENGTH_SHORT).show();
                    Intent i = new Intent(PrincipalActivity.this, LoginActivity.class);
                    startActivity(i);
                }
            }
        };
    }

    @Override
    public void onBackPressed() {
        int countFragment = getFragmentManager().getBackStackEntryCount();
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else if(countFragment!=0){
            getFragmentManager().popBackStack();
        } else{
            moveTaskToBack(true);
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.

        FragmentManager fm = getFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();

        int id = item.getItemId();

        switch(id){
            case R.id.menu_vehiculo:
                ft.replace(R.id.contentLayout,new MisVehiculosFragment(),"mis_vehiculos");
                break;
            case R.id.menu_mapa:
                ft.replace(R.id.contentLayout,new MapaFragment(),"mapa");
                break;
            case R.id.menu_historial:
                ft.replace(R.id.contentLayout,new HistorialFragment(),"historial");
                break;
            case R.id.menu_favoritos:
                ft.replace(R.id.contentLayout,new FavoritosFragment(),"favoritos");
                break;
            case R.id.menu_agregar_est:
                ft.replace(R.id.contentLayout,new AgregarEstacionamientoFragment(),"agregar_est");
                break;
            case R.id.menu_mis_solicitudes:
                ft.replace(R.id.contentLayout,new MisSolicitudesFragment(),"mis_solicitudes");
                break;
            case R.id.menu_actualizar_contrasena:
                ft.replace(R.id.contentLayout,new ActualizarContrasenaFragment(),"act_contra");
                break;
            case R.id.menu_aprobar_estacionamiento:
                ft.replace(R.id.contentLayout,new AprobarEstacionamientoFragment(),"act_contra");
                break;
            case R.id.menu_cerrar_sesion:
                FirebaseAuth.getInstance().signOut();
                LoginManager.getInstance().logOut();
                break;

        }

        if(id != R.id.menu_cerrar_sesion){
            ft.addToBackStack(null);
            ft.commit();
        }


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(mAuthListener);
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mAuthListener != null) {
            mAuth.removeAuthStateListener(mAuthListener);
        }
    }
}
