package com.diego.estacionate.viewholders;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.diego.estacionate.R;
import com.diego.estacionate.entidades.Estacionamiento;


public class AprobarEstViewHolder extends RecyclerView.ViewHolder {

    TextView tvNombre, tvDireccion, tvCapacidad, tvHorario, tvTipo, tvAltura, tvPrecio;
    public View itemView;

    public AprobarEstViewHolder(View itemView) {
        super(itemView);

        tvNombre = (TextView) itemView.findViewById(R.id.tvNombre);
        tvDireccion = (TextView) itemView.findViewById(R.id.tvDireccion);
        tvCapacidad = (TextView) itemView.findViewById(R.id.tvCapacidad);
        tvHorario = (TextView) itemView.findViewById(R.id.tvHorario);
        tvTipo = (TextView) itemView.findViewById(R.id.tvTipo);
        tvAltura = (TextView) itemView.findViewById(R.id.tvAltura);
        tvPrecio = (TextView) itemView.findViewById(R.id.tvPrecio);
        this.itemView = itemView;
    }

    public void verDetalles(final Estacionamiento estacionamiento, final Context context, View.OnClickListener listener) {
        tvNombre.setText(estacionamiento.getNombre());
        tvDireccion.setText(estacionamiento.getDireccion());
        tvCapacidad.setText(estacionamiento.getCapacidad()+" vehículos");
        tvHorario.setText(estacionamiento.getHorario());
        tvTipo.setText(estacionamiento.getTipo_estacionamiento());
        tvAltura.setText(estacionamiento.getAltura());
        if(estacionamiento.getAltura().equals("0")){ tvAltura.setText("Sin límite de altura"); }
        tvPrecio.setText(estacionamiento.getPrecio());
        if(estacionamiento.getPrecio().equals("0")){ tvPrecio.setText("Gratis"); }


        itemView.setOnClickListener(listener);


    }
}
