package com.diego.estacionate;


import android.app.Fragment;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.text.InputFilter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.diego.estacionate.entidades.Vehiculo;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;
import java.util.Map;


/**
 * A simple {@link Fragment} subclass.
 */
public class AgregarVehiculoFragment extends Fragment {

    private ProgressDialog mProgressDialog;

    EditText txtPatente, txtMarca, txtModelo;
    Button btnAgregar;
    private DatabaseReference mDatabase;

    public AgregarVehiculoFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_agregar_vehiculo, container, false);

        mDatabase = FirebaseDatabase.getInstance().getReference();

        txtPatente = (EditText)view.findViewById(R.id.txtPatente);
        txtMarca = (EditText)view.findViewById(R.id.txtMarca);
        txtModelo = (EditText)view.findViewById(R.id.txtModelo);
        btnAgregar = (Button)view.findViewById(R.id.btnAgregar);

        btnAgregar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                agregarVehiculo();
            }
        });

        txtPatente.setFilters(new InputFilter[] {new InputFilter.AllCaps()});

        return view;
    }

    private void agregarVehiculo() {
        boolean correcto = true;
        if(txtPatente.getText().toString().length()<1){
            txtPatente.setError("Debe ingresar la patente");
            correcto = false;
        }
        if(txtMarca.getText().toString().length()<1){
            txtMarca.setError("Debe ingresar la marca");
            correcto = false;
        }
        if(txtModelo.getText().toString().length()<1){
            txtModelo.setError("Debe ingresar el modelo");
            correcto = false;
        }
        if(correcto){
            showProgressDialog();
            String id_usuario=FirebaseAuth.getInstance().getCurrentUser().getUid();
            Vehiculo vehiculo = new Vehiculo();
            vehiculo.setPatente(txtPatente.getText().toString());
            vehiculo.setMarca(txtMarca.getText().toString());
            vehiculo.setModelo(txtModelo.getText().toString());
            String key = mDatabase.child("vehiculos")
                    .child(id_usuario)
                    .push().getKey();
            vehiculo.setId_vehiculo(key);

            Map<String, Object> childUpdates = new HashMap<>();
            childUpdates.put("/vehiculos/" + id_usuario + "/" + key, vehiculo.toMap());

            mDatabase.updateChildren(childUpdates);

            hideProgressDialog();
            Toast.makeText(getActivity(), "Vehículo agregado", Toast.LENGTH_SHORT).show();

            limpiar();

        }
    }

    private void limpiar() {
        txtPatente.setText("");
        txtMarca.setText("");
        txtModelo.setText("");
    }

    public void showProgressDialog() {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(getActivity());
            mProgressDialog.setCancelable(false);
            mProgressDialog.setMessage("Cargando...");
        }

        mProgressDialog.show();
    }

    public void hideProgressDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }

}
