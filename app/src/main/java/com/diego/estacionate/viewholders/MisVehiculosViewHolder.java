package com.diego.estacionate.viewholders;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.diego.estacionate.R;
import com.diego.estacionate.entidades.Estacionamiento;
import com.diego.estacionate.entidades.Vehiculo;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.Query;


public class MisVehiculosViewHolder extends RecyclerView.ViewHolder {

    public TextView tvPatente;
    public TextView tvMarcaModelo;
    public View itemView;

    public MisVehiculosViewHolder(View itemView) {
        super(itemView);

        tvPatente = (TextView) itemView.findViewById(R.id.tvPatente);
        tvMarcaModelo = (TextView) itemView.findViewById(R.id.tvMarcaModelo);
        this.itemView = itemView;
    }

    public void verDetalles(final Vehiculo vehiculo, final Context context, View.OnClickListener listener) {
        tvPatente.setText(vehiculo.getPatente());
        tvMarcaModelo.setText(vehiculo.getMarca()+" "+vehiculo.getModelo());


        itemView.setOnClickListener(listener);


    }


}
