package com.diego.estacionate;


import android.app.Activity;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.diego.estacionate.entidades.Estacionamiento;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;


/**
 * A simple {@link Fragment} subclass.
 */
public class AgregarEstacionamientoFragment extends Fragment {

    private ProgressDialog mProgressDialog;

    private EditText txtNombre,txtDireccion,txtCapacidad,txtHorario,txtAlturaMax,txtPrecio;
    private Spinner sp_tipo_est;
    private Button btnAgregar, btnTomarFoto, btnSubirFoto;
    private ImageView imgPrevia;
    private TextView tvImagenNoSeleccionada;

    private DatabaseReference mDatabase;

    private static int TAKE_PICTURE = 1;
    private static int SELECT_PICTURE = 2;
    private final String imgNombre = Environment.getExternalStorageDirectory() + "/fbaseImg.jpg";
    private String imgRutaFinal = "";

    private StorageReference storageRef;
    private FirebaseStorage storage;

    public AgregarEstacionamientoFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_agregar_estacionamiento, container, false);



        btnTomarFoto = (Button)view.findViewById(R.id.btnTomarFoto);
        btnTomarFoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent =  new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                Uri output = Uri.fromFile(new File(imgNombre));
                intent.putExtra(MediaStore.EXTRA_OUTPUT, output);
                startActivityForResult(intent, TAKE_PICTURE);
            }
        });

        btnSubirFoto = (Button)view.findViewById(R.id.btnSubirFoto);
        btnSubirFoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.INTERNAL_CONTENT_URI);
                startActivityForResult(intent, SELECT_PICTURE);
            }
        });

        imgPrevia = (ImageView)view.findViewById(R.id.imgPrevia);

        mDatabase= FirebaseDatabase.getInstance().getReference();

        tvImagenNoSeleccionada=(TextView)view.findViewById(R.id.tvImagenNoSeleccionada);

        txtNombre = (EditText)view.findViewById(R.id.txtNombre);
        txtDireccion = (EditText)view.findViewById(R.id.txtDireccion);
        txtCapacidad = (EditText)view.findViewById(R.id.txtCapacidad);
        txtHorario = (EditText)view.findViewById(R.id.txtHorario);
        txtAlturaMax = (EditText)view.findViewById(R.id.txtAlturaMax);
        txtPrecio = (EditText)view.findViewById(R.id.txtPrecio);

        sp_tipo_est = (Spinner)view.findViewById(R.id.sp_tipo_est);

        btnAgregar = (Button)view.findViewById(R.id.btnAgregar);
        btnAgregar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                agregarEst();
            }
        });

        return view;
    }

    private void agregarEst() {

        boolean correcto = true;
        if(txtNombre.getText().toString().length()<1){
            txtNombre.setError("Debe ingresar el nombre");
            correcto=false;
        }
        if(txtDireccion.getText().toString().length()<1){
            txtDireccion.setError("Debe ingresar la dirección");
            correcto=false;
        }
        if(txtCapacidad.getText().toString().length()<1){
            txtCapacidad.setError("Debe ingresar la capacidad");
            correcto=false;
        }
        if(txtHorario.getText().toString().length()<1){
            txtHorario.setError("Debe ingresar el horario");
            correcto=false;
        }
        if(txtAlturaMax.getText().toString().length()<1){
            txtAlturaMax.setError("Debe ingresar la altura máxima");
            correcto=false;
        }
        if(txtPrecio.getText().toString().length()<1){
            txtPrecio.setError("Debe ingresar el precio");
            correcto=false;
        }
        if(correcto){
            showProgressDialog();
            final Estacionamiento est = new Estacionamiento();
            est.setNombre(txtNombre.getText().toString());
            est.setDireccion(txtDireccion.getText().toString());
            est.setCapacidad(txtCapacidad.getText().toString());
            est.setHorario(txtHorario.getText().toString());
            est.setAltura(txtAlturaMax.getText().toString());
            est.setPrecio(txtPrecio.getText().toString());
            est.setTipo_estacionamiento(sp_tipo_est.getSelectedItem().toString());
            est.setId_usuario(FirebaseAuth.getInstance().getCurrentUser().getUid());
            est.setEstado("Pendiente");
            try {
                Geocoder geocoder = new Geocoder(getActivity(), Locale.getDefault());
                List<Address> addresses = geocoder.getFromLocationName(est.getDireccion(),1);
                if (addresses != null) {
                    est.setGeoX(String.valueOf(addresses.get(0).getLatitude()));
                    est.setGeoY(String.valueOf(addresses.get(0).getLongitude()));
                } else {
                    Log.d("GEOC","No se pudo cargar latlng");
                }
            } catch (Exception e) {
                Log.d("GEOC",e.getMessage());
            }

            final String key = mDatabase.child("estacionamientos").push().getKey();
            est.setId_estacionamiento(key);

            if(imgPrevia.getDrawable() == null){
                est.setPath_img("");

                Map<String, Object> childUpdates = new HashMap<>();
                childUpdates.put("/estacionamientos/" + key , est.toMap());

                mDatabase.updateChildren(childUpdates);
                hideProgressDialog();
                Toast.makeText(getActivity(), "Estacionamiento agregado, espere aprobación", Toast.LENGTH_SHORT).show();
                limpiar();
            }else {

                storage = FirebaseStorage.getInstance();
                storageRef = storage.getReferenceFromUrl("gs://estacionate-f78ce.appspot.com");
                StorageReference baseImagen = storageRef.child("estacionamientos/" + key + ".jpg");
                try {
                    InputStream stream = new FileInputStream(new File(imgRutaFinal));

                    UploadTask uploadTask = baseImagen.putStream(stream);
                    uploadTask.addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception exception) {
                            // Handle unsuccessful uploads
                            Toast.makeText(getActivity(), "Error al subir imagen, intente nuevamente", Toast.LENGTH_SHORT).show();
                        }
                    }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                            // taskSnapshot.getMetadata() contains file metadata such as size, content-type, and download URL.
                            Uri downloadUrl = taskSnapshot.getDownloadUrl();
                            //Toast.makeText(getActivity(), "Imagen subida", Toast.LENGTH_SHORT).show();
                            est.setPath_img(downloadUrl.toString());

                            Map<String, Object> childUpdates = new HashMap<>();
                            childUpdates.put("/estacionamientos/" + key , est.toMap());

                            mDatabase.updateChildren(childUpdates);
                            hideProgressDialog();
                            Toast.makeText(getActivity(), "Estacionamiento agregado, espere aprobación", Toast.LENGTH_SHORT).show();
                            limpiar();
                        }
                    });
                } catch (Exception e) {
                    hideProgressDialog();
                    Toast.makeText(getActivity(), "Error: "+e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

        }

    }

    private void limpiar(){
        txtNombre.setText("");
        txtDireccion.setText("");
        txtCapacidad.setText("");
        txtHorario.setText("");
        txtAlturaMax.setText("");
        txtPrecio.setText("");
        imgPrevia.setImageDrawable(null);
        imgRutaFinal="";
        tvImagenNoSeleccionada.setVisibility(View.VISIBLE);

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == -1) {
            //Here you can handle,do anything you want

            if (requestCode == TAKE_PICTURE) {
                /*if (data != null) {
                    if (data.hasExtra("data")) {
                        imgPrevia.setImageBitmap((Bitmap) data.getParcelableExtra("data"));
                    }
                } else {
                    imgPrevia.setImageBitmap(BitmapFactory.decodeFile(imgNombre));
                }
                tvImagenNoSeleccionada.setVisibility(View.INVISIBLE);
                */
                imgRutaFinal = imgNombre;

            } else if (requestCode == SELECT_PICTURE) {
                Uri selectedImage = data.getData();
                /*InputStream is;
                try {
                    is = getActivity().getContentResolver().openInputStream(selectedImage);
                    BufferedInputStream bis = new BufferedInputStream(is);
                    Bitmap bitmap = BitmapFactory.decodeStream(bis);
                    imgPrevia.setImageBitmap(bitmap);
                    tvImagenNoSeleccionada.setVisibility(View.INVISIBLE);


                } catch (FileNotFoundException e) {
                }*/
                imgRutaFinal = getRealPathFromURI(selectedImage);
            }
            Picasso.with(getActivity()).load(new File(imgRutaFinal))
                    .fit()
                    .centerInside()
                    .into(imgPrevia, new com.squareup.picasso.Callback() {
                        @Override
                        public void onSuccess() {
                            tvImagenNoSeleccionada.setVisibility(View.INVISIBLE);
                        }

                        @Override
                        public void onError() {

                        }
                    });
        }

    }

    public String getRealPathFromURI(Uri uri) {
        String[] projection = { MediaStore.Images.Media.DATA };
        @SuppressWarnings("deprecation")
        Cursor cursor = getActivity().managedQuery(uri, projection, null, null, null);
        int column_index = cursor
                .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    public void showProgressDialog() {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(getActivity());
            mProgressDialog.setCancelable(false);
            mProgressDialog.setMessage("Cargando...");
        }

        mProgressDialog.show();
    }

    public void hideProgressDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }
}
