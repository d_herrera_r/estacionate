package com.diego.estacionate.entidades;

import com.google.firebase.database.Exclude;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by diego on 25-11-2016.
 */

public class Vehiculo {

    private String id_vehiculo;
    private String patente;
    private String marca;
    private String modelo;

    public String getId_vehiculo() {
        return id_vehiculo;
    }

    public void setId_vehiculo(String id_vehiculo) {
        this.id_vehiculo = id_vehiculo;
    }

    public String getPatente() {
        return patente;
    }

    public void setPatente(String patente) {
        this.patente = patente;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public Vehiculo() {
    }

    @Exclude
    public Map<String, Object> toMap() {
        HashMap<String, Object> result = new HashMap<>();
        result.put("id_vehiculo",getId_vehiculo());
        result.put("patente",getPatente());
        result.put("marca",getMarca());
        result.put("modelo",getModelo());
        return result;
    }
}
