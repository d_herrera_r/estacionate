package com.diego.estacionate;


import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.diego.estacionate.entidades.Estacionamiento;
import com.diego.estacionate.viewholders.EstacionamientosViewHolder;
import com.diego.estacionate.viewholders.MisSolicitudesViewHolder;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;


/**
 * A simple {@link Fragment} subclass.
 */
public class MisSolicitudesFragment extends Fragment {

    private RecyclerView rvMisSolicitudes;

    private LinearLayoutManager mManager;
    private DatabaseReference mDatabase;
    private FirebaseRecyclerAdapter<Estacionamiento, MisSolicitudesViewHolder> mAdapter;

    public MisSolicitudesFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_mis_solicitudes, container, false);

        mDatabase = FirebaseDatabase.getInstance().getReference();

        rvMisSolicitudes = (RecyclerView)view.findViewById(R.id.rvMisSolicitudes);
        mManager = new LinearLayoutManager(getActivity());
        mManager.setReverseLayout(true);
        mManager.setStackFromEnd(true);
        rvMisSolicitudes.setLayoutManager(mManager);
        Query postsQuery = mDatabase.child("estacionamientos").orderByChild("id_usuario").equalTo(FirebaseAuth.getInstance().getCurrentUser().getUid());

        mAdapter = new FirebaseRecyclerAdapter<Estacionamiento, MisSolicitudesViewHolder>(Estacionamiento.class,
                R.layout.mis_solicitudes_item, MisSolicitudesViewHolder.class, postsQuery) {
            @Override
            protected void populateViewHolder(MisSolicitudesViewHolder viewHolder,final Estacionamiento model, int position) {

                try {
                    final DatabaseReference postRef = getRef(position);


                    viewHolder.verDetalles(model, getActivity());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };

        rvMisSolicitudes.setAdapter(mAdapter);

        return view;
    }

}
