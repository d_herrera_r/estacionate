package com.diego.estacionate;


import android.app.Fragment;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.EmailAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;



/**
 * A simple {@link Fragment} subclass.
 */
public class ActualizarContrasenaFragment extends Fragment {

    private ProgressDialog mProgressDialog;

    private EditText txtContrasenaAnterior, txtNuevaContarsena, txtRepNuevaContrasena;
    private Button btnActualizar;
    private FirebaseUser user;
    private final static String TAG = "ReAuth";

    public ActualizarContrasenaFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_actualizar_contrasena, container, false);

        user = FirebaseAuth.getInstance().getCurrentUser();

        txtContrasenaAnterior = (EditText)view.findViewById(R.id.txtContrasenaAnterior);
        txtNuevaContarsena = (EditText)view.findViewById(R.id.txtNuevaContrasena);
        txtRepNuevaContrasena = (EditText)view.findViewById(R.id.txtRepNuevaContrasena);

        btnActualizar = (Button) view.findViewById(R.id.btnActualizar);
        btnActualizar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                actualizarContrasena();
            }
        });

        return view;
    }

    private void actualizarContrasena() {

        boolean correcto = true;

        if(txtContrasenaAnterior.getText().toString().length()<1){
            txtContrasenaAnterior.setError("Debe ingresar la contraseña anterior");
            correcto=false;
        }
        if(txtNuevaContarsena.getText().toString().length()<1){
            txtNuevaContarsena.setError("Debe ingresar la nueva contraseña");
            correcto=false;
        }
        if(txtRepNuevaContrasena.getText().toString().length()<1){
            txtRepNuevaContrasena.setError("Debe repetir la nueva contraseña");
            correcto=false;
        }
        if(txtNuevaContarsena.getText().toString().length()<6){
            txtNuevaContarsena.setError("La contraseña debe tener mínimo 6 caracteres");
            correcto=false;
        }
        if(txtRepNuevaContrasena.getText().toString().length()<6){
            txtRepNuevaContrasena.setError("La contraseña debe tener mínimo 6 caracteres");
            correcto=false;
        }
        if(!txtNuevaContarsena.getText().toString().equals(txtRepNuevaContrasena.getText().toString())){
            txtRepNuevaContrasena.setError("Las contraseñas no coinciden");
            correcto=false;
        }

        if (correcto){

            showProgressDialog();
            AuthCredential credential = EmailAuthProvider
                    .getCredential(user.getEmail(), txtContrasenaAnterior.getText().toString());

            // Prompt the user to re-provide their sign-in credentials
            user.reauthenticate(credential)
                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if(task.isSuccessful()){
                                user.updatePassword(txtNuevaContarsena.getText().toString())
                                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                                            @Override
                                            public void onComplete(@NonNull Task<Void> task) {
                                                if (task.isSuccessful()) {
                                                    hideProgressDialog();
                                                    Toast.makeText(getActivity(), "Contraseña actualizada", Toast.LENGTH_SHORT).show();
                                                    limpiar();
                                                }else{
                                                    hideProgressDialog();
                                                    Toast.makeText(getActivity(), "Error: "+task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                                                }
                                            }
                                        });
                            }else{
                                hideProgressDialog();
                                txtContrasenaAnterior.setError("Contraseña inválida");
                            }
                        }
                    });
        }



    }

    private void limpiar() {

        txtContrasenaAnterior.setText("");
        txtNuevaContarsena.setText("");
        txtRepNuevaContrasena.setText("");
    }

    public void showProgressDialog() {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(getActivity());
            mProgressDialog.setCancelable(false);
            mProgressDialog.setMessage("Cargando...");
        }

        mProgressDialog.show();
    }

    public void hideProgressDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }

}
