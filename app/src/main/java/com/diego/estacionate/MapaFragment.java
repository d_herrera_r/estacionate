package com.diego.estacionate;


import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.diego.estacionate.entidades.*;
import com.diego.estacionate.viewholders.EstacionamientosViewHolder;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.sothree.slidinguppanel.SlidingUpPanelLayout;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import android.view.View.OnClickListener;

import com.sothree.slidinguppanel.SlidingUpPanelLayout.PanelState;


/**
 *
 *
 * A simple {@link Fragment} subclass.
 */
public class MapaFragment extends Fragment implements OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener {

    private ProgressDialog mProgressDialog;

    private SlidingUpPanelLayout mLayout;

    ListView lvEstacionamientos;
    private DatabaseReference dbReference;

    GoogleMap mapa;

    LocationRequest mLocationRequest;
    GoogleApiClient mGoogleApiClient;
    RecyclerView rvEstacionamientos;
    private LinearLayoutManager mManager;
    private DatabaseReference mDatabase;
    private FirebaseRecyclerAdapter<Estacionamiento, EstacionamientosViewHolder> mAdapter;

    private Geocoder geocoder;
    private List<Marker> markers;

    MapFragment mf;

    public MapaFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.fragment_mapa, container, false);

        showProgressDialog();

        geocoder = new Geocoder(getActivity(), Locale.getDefault());
        markers = new ArrayList<Marker>();

        FragmentManager fmMapa;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            fmMapa = getChildFragmentManager();
        } else {
            fmMapa = getFragmentManager();
        }

        mf = MapFragment.newInstance();

        fmMapa.beginTransaction().replace(R.id.mapaLayout, mf).commit();

        mf.getMapAsync(this);

        mDatabase = FirebaseDatabase.getInstance().getReference();

        rvEstacionamientos = (RecyclerView)view.findViewById(R.id.rvEstacionamientos);


        mLayout = (SlidingUpPanelLayout) view.findViewById(R.id.sliding_layout);
        mLayout.setAnchorPoint(0.6f);
        mLayout.addPanelSlideListener(new SlidingUpPanelLayout.PanelSlideListener() {
            @Override
            public void onPanelSlide(View panel, float slideOffset) {
                Log.i("MapaFragment", "onPanelSlide, offset " + slideOffset);
            }

            @Override
            public void onPanelStateChanged(View panel, PanelState previousState, PanelState newState) {
                Log.i("MapaFragment", "onPanelStateChanged " + newState);
            }
        });
        mLayout.setFadeOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                mLayout.setPanelState(PanelState.COLLAPSED);
            }
        });


        return view;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mapa = googleMap;
        mapa.getUiSettings().setZoomControlsEnabled(true);
        mapa.getUiSettings().setRotateGesturesEnabled(false);
        if (ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED || ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            mapa.setMyLocationEnabled(true);
        } else {
            Toast.makeText(getActivity(), "Dar permisos", Toast.LENGTH_SHORT).show();
        }
        buildGoogleApiClient();

        mGoogleApiClient.connect();

        cargarEstacionamientos();
    }

    private void cargarEstacionamientos() {
        mManager = new LinearLayoutManager(getActivity());
        mManager.setReverseLayout(true);
        mManager.setStackFromEnd(true);
        rvEstacionamientos.setLayoutManager(mManager);
        Query postsQuery = mDatabase.child("estacionamientos").orderByChild("estado").equalTo("Aprobado");
        mAdapter = new FirebaseRecyclerAdapter<Estacionamiento, EstacionamientosViewHolder>(Estacionamiento.class,
                R.layout.estacionamientos_item, EstacionamientosViewHolder.class, postsQuery) {
            @Override
            protected void populateViewHolder(EstacionamientosViewHolder viewHolder,final Estacionamiento model, int position) {

                try {
                    final DatabaseReference postRef = getRef(position);

                    agregarMarcador(model);

                    viewHolder.verDetalles(model, getActivity(), new OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Bundle datos = new Bundle();
                            datos.putString("uid", model.getId_estacionamiento());

                            Fragment frag = new DetallesEstacionamientoFragment();
                            frag.setArguments(datos);

                            //Toast.makeText(getActivity(), tvNombreLv.getText().toString(), Toast.LENGTH_SHORT).show();
                            FragmentManager fm = getFragmentManager();
                            FragmentTransaction ft = fm.beginTransaction();
                            ft.replace(R.id.contentLayout, frag, "detalles");


                            ft.addToBackStack(null);
                            ft.commit();
                        }
                    });

                    hideProgressDialog();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
        rvEstacionamientos.setAdapter(mAdapter);
    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        if (ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED || ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            Location mLastLocation = LocationServices.FusedLocationApi.getLastLocation(
                    mGoogleApiClient);
            if (mLastLocation != null) {
                //place marker at current position
                //mGoogleMap.clear();
                LatLng latLng = new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude());
                mapa.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng,16));
            }

            mLocationRequest = new LocationRequest();
            mLocationRequest.setInterval(5000); //5 seconds
            mLocationRequest.setFastestInterval(3000); //3 seconds
            mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
            //mLocationRequest.setSmallestDisplacement(0.1F); //1/10 meter

            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        }


    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onLocationChanged(Location location) {
        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
        //mapa.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng,16));
    }


    private void agregarMarcador(Estacionamiento est) {
        LatLng latLng = new LatLng(Double.parseDouble(est.getGeoX()),Double.parseDouble(est.getGeoY()));
        Marker marker = mapa.addMarker(new MarkerOptions().position(latLng).title(est.getNombre()));
        markers.add(marker);
    }

    public void showProgressDialog() {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(getActivity());
            mProgressDialog.setCancelable(false);
            mProgressDialog.setMessage("Cargando...");
        }

        mProgressDialog.show();
    }

    public void hideProgressDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }

}
