package com.diego.estacionate;


import android.app.AlertDialog;
import android.app.Fragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.diego.estacionate.entidades.Estacionamiento;
import com.diego.estacionate.viewholders.AprobarEstViewHolder;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;

import java.util.HashMap;
import java.util.Map;


/**
 * A simple {@link Fragment} subclass.
 */
public class AprobarEstacionamientoFragment extends Fragment {

    private LinearLayoutManager mManager;
    private DatabaseReference mDatabase;
    private FirebaseRecyclerAdapter<Estacionamiento, AprobarEstViewHolder> mAdapter;
    private RecyclerView rvPendientes;

    public AprobarEstacionamientoFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_aprobar_estacionamiento, container, false);

        rvPendientes = (RecyclerView) view.findViewById(R.id.rvPendientes);
        mDatabase = FirebaseDatabase.getInstance().getReference();
        cargarPendientes();


        return view;
    }

    private void cargarPendientes() {
        mManager = new LinearLayoutManager(getActivity());
        mManager.setReverseLayout(true);
        mManager.setStackFromEnd(true);
        rvPendientes.setLayoutManager(mManager);
        Query postsQuery = mDatabase.child("estacionamientos").orderByChild("estado").equalTo("Pendiente");
        mAdapter = new FirebaseRecyclerAdapter<Estacionamiento, AprobarEstViewHolder>(Estacionamiento.class,
                R.layout.pendiente_item, AprobarEstViewHolder.class, postsQuery) {
            @Override
            protected void populateViewHolder(AprobarEstViewHolder viewHolder,final Estacionamiento model, int position) {

                try {
                    final DatabaseReference postRef = getRef(position);

                    viewHolder.verDetalles(model, getActivity(), new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                            builder .setTitle("Actualizar estado")
                                    .setMessage("¿Qué desea realizar con el estacionamiento?")
                                    .setCancelable(false)
                                    .setNegativeButton("Rechazar",
                                            new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int id) {
                                                    mDatabase.child("estacionamientos")
                                                            .child(model.getId_estacionamiento())
                                                            .child("estado").setValue("Rechazado");


                                                    dialog.dismiss();
                                                    Toast.makeText(getActivity(), "Estacionamiento rechazado", Toast.LENGTH_SHORT).show();
                                                }
                                            })
                                    .setPositiveButton("Aprobar",
                                            new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int id) {
                                                    mDatabase.child("estacionamientos")
                                                            .child(model.getId_estacionamiento())
                                                            .child("estado").setValue("Aprobado");

                                                    dialog.dismiss();
                                                    Toast.makeText(getActivity(), "Estacionamiento aprobado", Toast.LENGTH_SHORT).show();

                                                }
                                            })
                                    .setNeutralButton("Cancelar", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.dismiss();
                                        }
                                    });
                            builder.show();


                        }
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
        rvPendientes.setAdapter(mAdapter);
    }

}
