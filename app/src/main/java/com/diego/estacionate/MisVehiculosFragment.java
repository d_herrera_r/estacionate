package com.diego.estacionate;


import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.app.Fragment;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.diego.estacionate.entidades.Estacionamiento;
import com.diego.estacionate.entidades.Vehiculo;
import com.diego.estacionate.viewholders.FavoritosViewHolder;
import com.diego.estacionate.viewholders.MisVehiculosViewHolder;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;


/**
 * A simple {@link Fragment} subclass.
 */
public class MisVehiculosFragment extends Fragment {

    FloatingActionButton btnAgregarVehiculo;
    private RecyclerView rvMisVehiculos;
    private LinearLayoutManager mManager;
    private DatabaseReference mDatabase;
    private FirebaseRecyclerAdapter<Vehiculo, MisVehiculosViewHolder> mAdapter;


    public MisVehiculosFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_mis_vehiculos, container, false);

        mDatabase = FirebaseDatabase.getInstance().getReference();

        btnAgregarVehiculo = (FloatingActionButton) view.findViewById(R.id.btnAgregarVehiculo);
        rvMisVehiculos = (RecyclerView)view.findViewById(R.id.rvMisVehiculos);

        cargarMisVehiculos();

        btnAgregarVehiculo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentManager fm = getFragmentManager();
                FragmentTransaction ft = fm.beginTransaction();
                ft.replace(R.id.contentLayout,new AgregarVehiculoFragment(),"agregar_vehiculo");
                ft.addToBackStack(null);
                ft.commit();
            }
        });

        return view;
    }

    private void cargarMisVehiculos() {
        mManager = new LinearLayoutManager(getActivity());
        mManager.setReverseLayout(true);
        mManager.setStackFromEnd(true);
        rvMisVehiculos.setLayoutManager(mManager);
        Query postsQuery = mDatabase.child("vehiculos")
                .child(FirebaseAuth.getInstance().getCurrentUser().getUid());
        mAdapter = new FirebaseRecyclerAdapter<Vehiculo, MisVehiculosViewHolder>(Vehiculo.class,
                R.layout.vehiculos_item, MisVehiculosViewHolder.class, postsQuery) {
            @Override
            protected void populateViewHolder(MisVehiculosViewHolder viewHolder,final Vehiculo model, int position) {

                try {
                    final DatabaseReference postRef = getRef(position);

                    viewHolder.verDetalles(model, getActivity(), new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                        }
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
        rvMisVehiculos.setAdapter(mAdapter);
    }

}
