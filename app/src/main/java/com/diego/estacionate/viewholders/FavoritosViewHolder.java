package com.diego.estacionate.viewholders;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.diego.estacionate.R;
import com.diego.estacionate.entidades.Estacionamiento;


public class FavoritosViewHolder extends RecyclerView.ViewHolder {

    public TextView tvNombre;
    public TextView tvDireccion;
    public View itemView;

    public FavoritosViewHolder(View itemView) {
        super(itemView);

        tvNombre = (TextView) itemView.findViewById(R.id.tvNombre);
        tvDireccion = (TextView) itemView.findViewById(R.id.tvDireccion);
        this.itemView = itemView;
    }

    public void verDetalles(final Estacionamiento estacionamiento, final Context context, View.OnClickListener listener) {
        tvNombre.setText(estacionamiento.getNombre());
        tvDireccion.setText(estacionamiento.getDireccion());


        itemView.setOnClickListener(listener);


    }
}
