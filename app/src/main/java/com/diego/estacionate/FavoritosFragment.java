package com.diego.estacionate;


import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.diego.estacionate.entidades.Estacionamiento;
import com.diego.estacionate.viewholders.EstacionamientosViewHolder;
import com.diego.estacionate.viewholders.FavoritosViewHolder;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;


/**
 * A simple {@link Fragment} subclass.
 */
public class FavoritosFragment extends Fragment {


    private LinearLayoutManager mManager;
    private DatabaseReference mDatabase;
    private FirebaseRecyclerAdapter<Estacionamiento, FavoritosViewHolder> mAdapter;
    private RecyclerView rvFavoritos;

    public FavoritosFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_favoritos, container, false);

        mDatabase = FirebaseDatabase.getInstance().getReference();
        rvFavoritos = (RecyclerView)view.findViewById(R.id.rvFavoritos);


        cargarFavoritos();
        return view;
    }

    private void cargarFavoritos() {
        mManager = new LinearLayoutManager(getActivity());
        mManager.setReverseLayout(true);
        mManager.setStackFromEnd(true);
        rvFavoritos.setLayoutManager(mManager);
        Query postsQuery = mDatabase.child("favoritos")
                .child(FirebaseAuth.getInstance().getCurrentUser().getUid());
        mAdapter = new FirebaseRecyclerAdapter<Estacionamiento, FavoritosViewHolder>(Estacionamiento.class,
                R.layout.favoritos_item, FavoritosViewHolder.class, postsQuery) {
            @Override
            protected void populateViewHolder(FavoritosViewHolder viewHolder,final Estacionamiento model, int position) {

                try {
                    final DatabaseReference postRef = getRef(position);

                    viewHolder.verDetalles(model, getActivity(), new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Bundle datos = new Bundle();
                            datos.putString("uid", model.getId_estacionamiento());

                            Fragment frag = new DetallesEstacionamientoFragment();
                            frag.setArguments(datos);

                            FragmentManager fm = getFragmentManager();
                            FragmentTransaction ft = fm.beginTransaction();
                            ft.replace(R.id.contentLayout, frag, "detalles");


                            ft.addToBackStack(null);
                            ft.commit();
                        }
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
        rvFavoritos.setAdapter(mAdapter);
    }

}
