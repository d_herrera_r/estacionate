package com.diego.estacionate;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;

import static com.facebook.FacebookSdk.getApplicationContext;

public class LoginActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());

        setContentView(R.layout.activity_login);


    }

    @Override
    public void onBackPressed() {
        int countFragment = getFragmentManager().getBackStackEntryCount();
        if(countFragment == 0){
            moveTaskToBack(true);
        } else{
            getFragmentManager().popBackStack();
        }
    }
}
