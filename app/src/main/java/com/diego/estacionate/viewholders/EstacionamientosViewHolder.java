package com.diego.estacionate.viewholders;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.diego.estacionate.R;
import com.diego.estacionate.entidades.Estacionamiento;
import com.squareup.picasso.Picasso;



public class EstacionamientosViewHolder extends RecyclerView.ViewHolder {

    public TextView tvNombre;
    public TextView tvDireccion;
    public View itemView;

    public EstacionamientosViewHolder(View itemView) {
        super(itemView);

        tvNombre = (TextView) itemView.findViewById(R.id.tvNombre);
        tvDireccion = (TextView) itemView.findViewById(R.id.tvDireccion);
        this.itemView = itemView;
    }

    public void verDetalles(final Estacionamiento estacionamiento, final Context context, View.OnClickListener listener) {
        tvNombre.setText(estacionamiento.getNombre());
        tvDireccion.setText(estacionamiento.getDireccion());


        itemView.setOnClickListener(listener);


    }
}
