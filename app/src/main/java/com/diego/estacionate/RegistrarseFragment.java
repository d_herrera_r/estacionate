package com.diego.estacionate;


import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.diego.estacionate.entidades.Usuario;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthUserCollisionException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;



/**
 * A simple {@link Fragment} subclass.
 */
public class RegistrarseFragment extends Fragment {

    private ProgressDialog mProgressDialog;

    private static final String TAG = "RegistrarFBase";
    Button btnCrearCuenta,btnTomarFoto, btnSubirFoto;
    EditText txtCorreo, txtNombre, txtContrasena, txtRepContrasena;
    private ImageView imgPrevia;
    private TextView tvImagenNoSeleccionada;

    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    private DatabaseReference mDatabase;

    private static int TAKE_PICTURE = 1;
    private static int SELECT_PICTURE = 2;
    private final String imgNombre = Environment.getExternalStorageDirectory() + "/fBaseUser.jpg";
    private String imgRutaFinal = "";

    private StorageReference storageRef;
    private FirebaseStorage storage;


    public RegistrarseFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_registrarse, container, false);

        mDatabase = FirebaseDatabase.getInstance().getReference();

        mAuth = FirebaseAuth.getInstance();

        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user != null) {
                    // User is signed in
                    Log.d(TAG, "onAuthStateChanged:signed_in:" + user.getUid());



                } else {
                    // User is signed out
                    Log.d(TAG, "onAuthStateChanged:signed_out");
                }
            }
        };


        txtCorreo = (EditText) view.findViewById(R.id.txtCorreo);
        txtNombre = (EditText)view.findViewById(R.id.txtNombre);
        txtContrasena = (EditText) view.findViewById(R.id.txtContrasena);
        txtRepContrasena = (EditText) view.findViewById(R.id.txtRepContrasena);

        btnCrearCuenta = (Button) view.findViewById(R.id.btnCrearCuenta);

        btnCrearCuenta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                crearCuenta();
            }
        });

        btnTomarFoto = (Button)view.findViewById(R.id.btnTomarFoto);
        btnTomarFoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent =  new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                Uri output = Uri.fromFile(new File(imgNombre));
                intent.putExtra(MediaStore.EXTRA_OUTPUT, output);
                startActivityForResult(intent, TAKE_PICTURE);
            }
        });

        btnSubirFoto = (Button)view.findViewById(R.id.btnSubirFoto);
        btnSubirFoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.INTERNAL_CONTENT_URI);
                startActivityForResult(intent, SELECT_PICTURE);
            }
        });

        imgPrevia = (ImageView)view.findViewById(R.id.imgPrevia);

        tvImagenNoSeleccionada=(TextView)view.findViewById(R.id.tvImagenNoSeleccionada);

        return view;
    }

    private void crearCuenta() {
        boolean correcto = true;
        if(txtCorreo.getText().toString().length()<1){
            txtCorreo.setError("Debe ingresar el correo");
            correcto = false;
        }
        if(txtNombre.getText().toString().length()<1){
            txtNombre.setError("Debe ingresar el nombre");
            correcto = false;
        }
        if(txtContrasena.getText().toString().length()<1){
            txtContrasena.setError("Debe ingresar la contraseña");
            correcto = false;
        }
        if(txtContrasena.getText().toString().length()<6){
            txtContrasena.setError("La contraseña debe tener mínimo 6 caracteres");
            correcto = false;
        }
        if(txtRepContrasena.getText().toString().length()<1){
            txtRepContrasena.setError("Debe repetir la contraseña");
            correcto = false;
        }
        if(txtRepContrasena.getText().toString().length()<6){
            txtRepContrasena.setError("La contraseña debe tener mínimo 6 caracteres");
            correcto = false;
        }
        if(!txtContrasena.getText().toString().equals(txtRepContrasena.getText().toString())){
            txtRepContrasena.setError("Contraseñas no coinciden");
            correcto = false;
        }
        if(correcto){
            showProgressDialog();

            mAuth.createUserWithEmailAndPassword(txtCorreo.getText().toString(), txtContrasena.getText().toString())
                    .addOnCompleteListener(getActivity(), new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            Log.d(TAG, "createUserWithEmail:onComplete:" + task.isSuccessful());

                            // If sign in fails, display a message to the user. If sign in succeeds
                            // the auth state listener will be notified and logic to handle the
                            // signed in user can be handled in the listener.
                            if (!task.isSuccessful()) {
                                Log.d(TAG, "createUserWithEmail:onErrorTask:"+task.getException());
                                try {
                                    throw task.getException();
                                } catch(FirebaseAuthUserCollisionException e) {
                                    if(e.getErrorCode().equals("ERROR_EMAIL_ALREADY_IN_USE")){
                                        Toast.makeText(getActivity(), "Error, email ya se encuentra registrado", Toast.LENGTH_SHORT).show();
                                    }
                                    if(e.getErrorCode().equals("ERROR_ACCOUNT_EXISTS_WITH_DIFFERENT_CREDENTIAL")){
                                        Toast.makeText(getActivity(), "Error, email registrado como cuenta de facebook", Toast.LENGTH_SHORT).show();
                                    }
                                } catch(Exception e) {
                                    Log.e(TAG, e.getMessage());
                                }

                                hideProgressDialog();

                            }else{
                                final FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                                final Usuario usuario = new Usuario();
                                usuario.setCorreo(user.getEmail());
                                usuario.setNombre(txtNombre.getText().toString());
                                usuario.setId_usuario(user.getUid());

                                if(imgPrevia.getDrawable() == null){
                                    usuario.setPath_img("");
                                    actualizarUsuario(usuario, user);

                                }else {

                                    storage = FirebaseStorage.getInstance();
                                    storageRef = storage.getReferenceFromUrl("gs://estacionate-f78ce.appspot.com");
                                    StorageReference baseImagen = storageRef.child("usuarios/" + user.getUid() + ".jpg");
                                    try {
                                        InputStream stream = new FileInputStream(new File(imgRutaFinal));

                                        UploadTask uploadTask = baseImagen.putStream(stream);
                                        uploadTask.addOnFailureListener(new OnFailureListener() {
                                            @Override
                                            public void onFailure(@NonNull Exception exception) {
                                                // Handle unsuccessful uploads

                                                hideProgressDialog();
                                                Toast.makeText(getActivity(), "Error al subir imagen, intente nuevamente", Toast.LENGTH_SHORT).show();
                                            }
                                        }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                                            @Override
                                            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                                                // taskSnapshot.getMetadata() contains file metadata such as size, content-type, and download URL.
                                                Uri downloadUrl = taskSnapshot.getDownloadUrl();
                                                //Toast.makeText(getActivity(), "Imagen subida", Toast.LENGTH_SHORT).show();
                                                usuario.setPath_img(downloadUrl.toString());

                                                actualizarUsuario(usuario, user);

                                            }
                                        });
                                    } catch (Exception e) {
                                        hideProgressDialog();
                                        Toast.makeText(getActivity(), "Error: "+e.getMessage(), Toast.LENGTH_SHORT).show();

                                    }
                                }
                            }

                        }
                    });
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(mAuthListener);
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mAuthListener != null) {
            mAuth.removeAuthStateListener(mAuthListener);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == -1) {
            //Here you can handle,do anything you want

            if (requestCode == TAKE_PICTURE) {
                /*if (data != null) {
                    if (data.hasExtra("data")) {
                        imgPrevia.setImageBitmap((Bitmap) data.getParcelableExtra("data"));
                    }
                } else {
                    imgPrevia.setImageBitmap(BitmapFactory.decodeFile(imgNombre));
                }
                tvImagenNoSeleccionada.setVisibility(View.INVISIBLE);
                */
                imgRutaFinal = imgNombre;

            } else if (requestCode == SELECT_PICTURE) {
                Uri selectedImage = data.getData();
                /*InputStream is;
                try {
                    is = getActivity().getContentResolver().openInputStream(selectedImage);
                    BufferedInputStream bis = new BufferedInputStream(is);
                    Bitmap bitmap = BitmapFactory.decodeStream(bis);
                    imgPrevia.setImageBitmap(bitmap);
                    tvImagenNoSeleccionada.setVisibility(View.INVISIBLE);


                } catch (FileNotFoundException e) {
                }*/
                imgRutaFinal = getRealPathFromURI(selectedImage);
            }
            Picasso.with(getActivity()).load(new File(imgRutaFinal))
                    .fit()
                    .centerInside()
                    .into(imgPrevia, new com.squareup.picasso.Callback() {
                        @Override
                        public void onSuccess() {
                            tvImagenNoSeleccionada.setVisibility(View.INVISIBLE);
                        }

                        @Override
                        public void onError() {

                        }
                    });
        }

    }

    public String getRealPathFromURI(Uri uri) {
        String[] projection = { MediaStore.Images.Media.DATA };
        @SuppressWarnings("deprecation")
        Cursor cursor = getActivity().managedQuery(uri, projection, null, null, null);
        int column_index = cursor
                .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }


    private void limpiar(){
        txtNombre.setText("");
        txtCorreo.setText("");
        txtContrasena.setText("");
        txtRepContrasena.setText("");
        imgPrevia.setImageDrawable(null);
    }

    private void actualizarUsuario(Usuario usuario, FirebaseUser user){
        UserProfileChangeRequest profileUpdates = new UserProfileChangeRequest.Builder()
                .setDisplayName(usuario.getNombre())
                .setPhotoUri(Uri.parse(usuario.getPath_img()))
                .build();

        user.updateProfile(profileUpdates)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            hideProgressDialog();
                            limpiar();
                            Toast.makeText(getActivity(), "Cuenta creada", Toast.LENGTH_SHORT).show();
                            FirebaseAuth.getInstance().signOut();


                        }else{
                            hideProgressDialog();
                            Toast.makeText(getActivity(), "Error: "+task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

    public void showProgressDialog() {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(getActivity());
            mProgressDialog.setCancelable(false);
            mProgressDialog.setMessage("Cargando...");
        }

        mProgressDialog.show();
    }

    public void hideProgressDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }

}
