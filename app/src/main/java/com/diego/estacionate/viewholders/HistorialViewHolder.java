package com.diego.estacionate.viewholders;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.diego.estacionate.R;
import com.diego.estacionate.entidades.Estacionamiento;
import com.google.android.gms.vision.text.Text;


public class HistorialViewHolder extends RecyclerView.ViewHolder {

    public TextView tvNombre;
    public TextView tvDireccion;
    public TextView tvFechaUso;
    public View itemView;

    public HistorialViewHolder(View itemView) {
        super(itemView);

        tvNombre = (TextView) itemView.findViewById(R.id.tvNombre);
        tvDireccion = (TextView) itemView.findViewById(R.id.tvDireccion);
        tvFechaUso = (TextView) itemView.findViewById(R.id.tvFechaUso);
        this.itemView = itemView;
    }

    public void verDetalles(final Estacionamiento estacionamiento, final Context context, View.OnClickListener listener) {
        tvNombre.setText(estacionamiento.getNombre());
        tvDireccion.setText(estacionamiento.getDireccion());
        tvFechaUso.setText(estacionamiento.getFecha_uso());


        itemView.setOnClickListener(listener);


    }
}
