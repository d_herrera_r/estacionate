package com.diego.estacionate.entidades;

import com.google.firebase.database.Exclude;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Diego on 04-11-2016.
 */

public class Usuario {

    private String id_usuario;
    private String nombre;
    private String correo;
    private String path_img;
    private ArrayList<Estacionamiento> historial;
    private ArrayList<Estacionamiento> favoritos;

    public String getPath_img() {
        return path_img;
    }

    public void setPath_img(String path_img) {
        this.path_img = path_img;
    }

    public ArrayList<Estacionamiento> getHistorial() {
        return historial;
    }

    public void setHistorial(ArrayList<Estacionamiento> historial) {
        this.historial = historial;
    }

    public ArrayList<Estacionamiento> getFavoritos() {
        return favoritos;
    }

    public void setFavoritos(ArrayList<Estacionamiento> favoritos) {
        this.favoritos = favoritos;
    }

    public String getId_usuario() {
        return id_usuario;
    }

    public void setId_usuario(String id_usuario) {
        this.id_usuario = id_usuario;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }



    public Usuario(String id_usuario, String nombre, String correo, String contrasena) {
        setId_usuario(id_usuario);
        setNombre(nombre);
        setCorreo(correo);
    }

    public Usuario() {
    }

    @Exclude
    public Map<String, Object> toMap() {
        HashMap<String, Object> result = new HashMap<>();
        result.put("correo",getCorreo());
        result.put("nombre",getNombre());
        result.put("path_img",getPath_img());
        result.put("id_usuario", getId_usuario());
        return result;
    }
}
