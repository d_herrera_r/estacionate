package com.diego.estacionate;


import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import com.diego.estacionate.entidades.Estacionamiento;
import com.diego.estacionate.viewholders.HistorialViewHolder;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 */
public class HistorialFragment extends Fragment {

    private LinearLayoutManager mManager;
    private DatabaseReference mDatabase;
    private FirebaseRecyclerAdapter<Estacionamiento, HistorialViewHolder> mAdapter;
    private RecyclerView rvHistorial;

    public HistorialFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.fragment_historial, container, false);

        rvHistorial = (RecyclerView) view.findViewById(R.id.rvHistorial);
        mDatabase = FirebaseDatabase.getInstance().getReference();
        cargarHistorial();

        return view;
    }

    private void cargarHistorial() {
        mManager = new LinearLayoutManager(getActivity());
        mManager.setReverseLayout(true);
        mManager.setStackFromEnd(true);
        rvHistorial.setLayoutManager(mManager);
        Query postsQuery = mDatabase.child("historial")
                .child(FirebaseAuth.getInstance().getCurrentUser().getUid());
        mAdapter = new FirebaseRecyclerAdapter<Estacionamiento, HistorialViewHolder>(Estacionamiento.class,
                R.layout.historial_item, HistorialViewHolder.class, postsQuery) {
            @Override
            protected void populateViewHolder(HistorialViewHolder viewHolder,final Estacionamiento model, int position) {

                try {
                    final DatabaseReference postRef = getRef(position);

                    viewHolder.verDetalles(model, getActivity(), new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            Bundle datos = new Bundle();
                            datos.putString("uid", model.getId_estacionamiento());

                            Fragment frag = new DetallesEstacionamientoFragment();
                            frag.setArguments(datos);

                            FragmentManager fm = getFragmentManager();
                            FragmentTransaction ft = fm.beginTransaction();
                            ft.replace(R.id.contentLayout, frag, "detalles");


                            ft.addToBackStack(null);
                            ft.commit();
                        }
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
        rvHistorial.setAdapter(mAdapter);
    }

}
