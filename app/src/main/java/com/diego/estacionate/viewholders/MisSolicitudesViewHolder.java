package com.diego.estacionate.viewholders;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.diego.estacionate.R;
import com.diego.estacionate.entidades.Estacionamiento;


public class MisSolicitudesViewHolder extends RecyclerView.ViewHolder {

    public TextView tvNombre;
    public TextView tvDireccion;
    public TextView tvEstado;
    public View itemView;

    public MisSolicitudesViewHolder(View itemView) {
        super(itemView);

        tvNombre = (TextView) itemView.findViewById(R.id.tvNombre);
        tvDireccion = (TextView) itemView.findViewById(R.id.tvDireccion);
        tvEstado = (TextView) itemView.findViewById(R.id.tvEstado);
        this.itemView = itemView;
    }

    public void verDetalles(final Estacionamiento estacionamiento, final Context context) {
        tvNombre.setText(estacionamiento.getNombre());
        tvDireccion.setText(estacionamiento.getDireccion());
        tvEstado.setText(estacionamiento.getEstado());
        if(estacionamiento.getEstado().equals("Aprobado")){
            tvEstado.setTextColor(Color.parseColor("#56ce64"));
        }else if(estacionamiento.getEstado().equals("Rechazado")){
            tvEstado.setTextColor(Color.parseColor("#c12626"));
        }else{
            tvEstado.setTextColor(Color.parseColor("#c1c425"));
        }

    }
}
